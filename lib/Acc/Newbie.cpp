//===--- Newbie.cpp - Common newbie mistake tests -------------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the GNU General Public License.
// See LICENSE.TXT in this directory for details.
//
//===----------------------------------------------------------------------===//
//
// Static analyzer for common newbie mistakes:
// - assignments instead of comparison in conditions
// - indirect use of global variables (may produce surprising bugs)
//
//===----------------------------------------------------------------------===//

#include "clang/Basic/Diagnostic.h"
#include "clang/Basic/SourceManager.h"
#include "clang/Basic/SourceLocation.h"
#include "clang/AST/StmtVisitor.h"
#include "clang/AST/APValue.h"
#include "clang/AST/ASTContext.h"
#include "clang/Analysis/LocalCheckers.h"
#include "llvm/Support/Compiler.h"
#include "llvm/ADT/DenseSet.h"

using namespace clang;

namespace {
/*! \brief Find identifier of the variable which gets modified
 */
class VISIBILITY_HIDDEN DeclRefFinder
  : public StmtVisitor<DeclRefFinder, ValueDecl *> {

  DeclRefExpr *found;
public:
  DeclRefFinder() : found(NULL) { }

  SourceLocation getLocation() { return found ? found->getLocStart() : SourceLocation(); }

  /// Simple variable
  ValueDecl *VisitDeclRefExpr(DeclRefExpr *R) {
    found = R;
    return dyn_cast<ValueDecl>(R->getDecl());
  }

  /// (foo)
  ValueDecl *VisitParenExpr(ParenExpr *S) {
    return Visit(S->getSubExpr());
  }

  /// &foo
  ValueDecl *VisitUnaryAddrOf(UnaryOperator *S) {
    return Visit(S->getSubExpr());
  }

  /// Binary operators: ptr = ptr OR ptr op int OR int op ptr
  ValueDecl *VisitBinaryOperator(BinaryOperator *S) {
    Expr *lsub = S->getLHS()->IgnoreParens();
    Expr *rsub = S->getRHS()->IgnoreParens();
    QualType lt = lsub->getType();
    QualType rt = rsub->getType();

    // right hand side first to prevent bogus reports from
    // expressions like *(foo = bar) = baz;
    if (rt->isPointerType() || rt->isArrayType()) {
      return Visit(rsub);
    } else if (lt->isPointerType() || lt->isArrayType()) {
      return Visit(lsub);
    }

    found = NULL;
    return NULL;
  }

  /// Pointer casts. Quit on non-pointer to pointer cast
  ValueDecl *VisitCastExpr(CastExpr *S) {
    Expr *sub = S->getSubExpr()->IgnoreParens();
    QualType t = sub->getType();
    if (t->isPointerType() || t->isArrayType()) {
      return Visit(sub);
    }

    found = NULL;
    return NULL;
  }

  /// Pointer casts. Quit on non-pointer to pointer cast
  ValueDecl *VisitImplicitCastExpr(ImplicitCastExpr *S) {
    Expr *sub = S->getSubExpr()->IgnoreParens();
    QualType t = sub->getType();
    if (t->isPointerType() || t->isArrayType()) {
      return Visit(sub);
    }

    found = NULL;
    return NULL;
  }

  /// foo[bar]
  ValueDecl *VisitArraySubscriptExpr(ArraySubscriptExpr *S) {
    return Visit(S->getBase());
  }

  /// No/unknown lvalue identifier
  ValueDecl *VisitStmt(Stmt *S) {
    found = NULL;
    return NULL;
  }

#define UNARYOPERATOR(CLASS) \
  ValueDecl *Visit ## CLASS(UnaryOperator *S) { \
    Expr *sub = S->getSubExpr()->IgnoreParens(); \
    QualType t = sub->getType(); \
    if (t->isPointerType() || t->isArrayType()) { \
      return Visit(sub); \
    } \
    found = NULL; \
    return NULL; \
  }

  UNARYOPERATOR(UnaryPostInc)
  UNARYOPERATOR(UnaryPostDec)
  UNARYOPERATOR(UnaryPreInc)
  UNARYOPERATOR(UnaryPreDec)
  UNARYOPERATOR(UnaryDeref)
#undef UNARYOPERATOR
};

class VISIBILITY_HIDDEN CondAssign
  : public StmtVisitor<CondAssign, void> {

  Diagnostic &D;
  SourceManager &mgr;

  unsigned Dyrmt; // Did you really mean that?

public:
  CondAssign(Diagnostic &d, SourceManager &m) : D(d), mgr(m),
  Dyrmt(D.getCustomDiagID(Diagnostic::SilentError, "Assignment in condition. Did you mean == instead?")) { }

  void VisitBinAssign(BinaryOperator *S) {
    D.Report(FullSourceLoc(S->getOperatorLoc(), mgr), Dyrmt);
  }

  void VisitChildren(Stmt *S) {
    for(Stmt::child_iterator I = S->child_begin(), E = S->child_end(); I != E; ++I) {
      if (*I) {
        Visit(*I);
      }
    }
  }

  void VisitStmt(Stmt *S) {
    VisitChildren(S);
  }
};

/*! Side effect usage checker
 */
class VISIBILITY_HIDDEN NewbieCallback
  : public StmtVisitor<NewbieCallback> {

  typedef StmtVisitor<NewbieCallback> Parent;

  Diagnostic &D;
  SourceManager &mgr;
  ASTContext &C;

  // Diagnostic messages
  const unsigned NoVar;
  const unsigned UnsafeExpr;
  const unsigned FloatEq;
  const unsigned SignedBitop;
  const unsigned BadShift;

public:
  NewbieCallback(Diagnostic &d, SourceManager &m, ASTContext &Ctx) : D(d), mgr(m), C(Ctx),
  NoVar(D.getCustomDiagID(Diagnostic::Warning, "Couldn't find variable identifier")),
  UnsafeExpr(D.getCustomDiagID(Diagnostic::SilentError, "Indirect use of global variables may produce weird errors")),
  FloatEq(D.getCustomDiagID(Diagnostic::SilentError, "Floating point numbers cannot be tested for equality due to inaccurate representation")),
  SignedBitop(D.getCustomDiagID(Diagnostic::Warning, "Signed bit operation results may vary with different compilers")),
  BadShift(D.getCustomDiagID(Diagnostic::SilentError, "Invalid shifting count, result is undefined")) { }

  void VisitChildren(Stmt *S) {
    for(Stmt::child_iterator I=S->child_begin(), E = S->child_end(); I != E; ++I) {
      if (*I) {
        Visit(*I);
      }
    }
  }

  /// &foo
  void VisitUnaryAddrOf(UnaryOperator *S) {
    DeclRefFinder F;
    ValueDecl *V = F.Visit(S->getSubExpr());

    if (!V) {
      D.Report(FullSourceLoc(S->getLocStart(), mgr), NoVar);
    } else if (V->isDefinedOutsideFunctionOrMethod()) {
      D.Report(FullSourceLoc(S->getLocStart(), mgr), UnsafeExpr);
    }

    VisitChildren(S);
  }

  /// Condition handler
  void VisitCondition(Expr *S) {
    CondAssign C(D, mgr);
    C.Visit(S);
  }

  /// Check that floats are not compared using == (unsafe)
  void VisitBinEQ(BinaryOperator *S) {
    if (S->getLHS()->getType()->isFloatingType()) {
      D.Report(FullSourceLoc(S->getOperatorLoc(), mgr), FloatEq);
    }

    VisitChildren(S);
  }

  /// Check that floats are not compared using != (unsafe)
  void VisitBinNE(BinaryOperator *S) {
    if (S->getLHS()->getType()->isFloatingType()) {
      D.Report(FullSourceLoc(S->getOperatorLoc(), mgr), FloatEq);
    }

    VisitChildren(S);
  }

  /// make sure that S is either unsigned or constant greater than or equal to 0
  bool isReallySigned(Expr *S) {
    if (!S->getType()->isSignedIntegerType()) {
      return false;
    }

    clang::Expr::Expr::EvalResult ret;
    if (!S->Evaluate(ret, C)) {
      return true;
    }

    return !ret.Val.getInt().sge(llvm::APInt(32,0,true));
  }

  /// make sure that shift count is in [0;LHS bit width-1] interval if constant
  bool badShiftRHS(QualType LHSType, Expr *RHS) {
    clang::Expr::Expr::EvalResult ret;
    if (!RHS->Evaluate(ret, C)) {
      return false;
    }

    llvm::APSInt tmp = ret.Val.getInt();
//    return tmp.slt(llvm::APInt(32,0)) || tmp.sge(llvm::APInt(32, C.getTypeSize(LHSType)));
    return tmp.isNegative() || tmp.sge(llvm::APInt(32, C.getTypeSize(LHSType)));
  }

  /// Check that bit negations are done only on unsigned types
  void VisitUnaryNot(UnaryOperator *S) {
    if (isReallySigned(S->getSubExpr())) {
      D.Report(FullSourceLoc(S->getOperatorLoc(), mgr), SignedBitop);
    }

    VisitChildren(S);
  }

  /// Pass throught the entire statement subtree
  void VisitStmt(Stmt *S) {
    VisitChildren(S);
  }

// check bitshifts
#define BITSHIFT(CLASS) \
  void Visit ## CLASS(BinaryOperator *S) { \
    if (isReallySigned(S->getLHS())) { \
      D.Report(FullSourceLoc(S->getOperatorLoc(), mgr), SignedBitop);\
    } \
    if (badShiftRHS(S->getLHS()->getType(), S->getRHS())) { \
      D.Report(FullSourceLoc(S->getOperatorLoc(), mgr), BadShift);\
    }\
    VisitChildren(S); \
  }

  BITSHIFT(BinShl)
  BITSHIFT(BinShr)
  BITSHIFT(BinShlAssign)
  BITSHIFT(BinShrAssign)
#undef BITSHIFT

// check bit operations
#define BITOP(CLASS) \
  void Visit ## CLASS(BinaryOperator *S) { \
    if (isReallySigned(S->getLHS()) || isReallySigned(S->getRHS())) { \
      D.Report(FullSourceLoc(S->getOperatorLoc(), mgr), SignedBitop);\
    } \
    VisitChildren(S); \
  }

  BITOP(BinAnd)
  BITOP(BinXor)
  BITOP(BinOr)
  BITOP(BinAndAssign)
  BITOP(BinXorAssign)
  BITOP(BinOrAssign)
#undef BITOP

#define CONDITION(CLASS) \
  void Visit ## CLASS(CLASS *S) { \
    VisitCondition(S->getCond()); \
    VisitChildren(S); \
  }

  CONDITION(IfStmt)
  CONDITION(ForStmt)
  CONDITION(DoStmt)
  CONDITION(WhileStmt)
  CONDITION(ConditionalOperator)
#undef CONDITION
};
}

namespace clang {
/// Execute side effect check on code block
void CheckNewbie(Diagnostic &Diags, SourceManager &mgr, ASTContext &Ctx, Stmt *Body) {
  NewbieCallback C(Diags, mgr, Ctx);
  C.Visit(Body);
}
}
