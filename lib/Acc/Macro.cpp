//===--- Macro.cpp - Side effect safety static check ----------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the GNU General Public License.
// See LICENSE.TXT in this directory for details.
//
//===----------------------------------------------------------------------===//
//
// Static analyzer for testing macro usage safety
//
//===----------------------------------------------------------------------===//

#include "clang/Basic/SourceManager.h"
#include "clang/Basic/Diagnostic.h"
#include "clang/Lex/MacroInfo.h"
#include "clang/Lex/Preprocessor.h"
#include "clang/Lex/PPCallbacks.h"

namespace {
bool isArg(const clang::MacroInfo *MI, const clang::Token &tok) {
  return tok.getIdentifierInfo() && MI->getArgumentNum(tok.getIdentifierInfo()) >= 0;
}

class MacroNewbie : public clang::PPCallbacks {
  clang::Diagnostic &Diag;
  clang::Preprocessor &PP;
  clang::SourceManager &mgr;
  const unsigned NoArgParen;

public:
  MacroNewbie(clang::Preprocessor &p, clang::Diagnostic &d, clang::SourceManager &m) : Diag(d), PP(p), mgr(m),
  NoArgParen(Diag.getCustomDiagID(clang::Diagnostic::Warning, "No parentheses around macro argument expansion")) {}

  /*! \brief Check that all macro arguments expanded as code
   * (except __VA_ARGS__) are enclosed in parentheses
   *
   * The state machine:
   * 0 = starting state, everything ok
   * 1 = last token was left parenthesis
   * 2 = last token was # or ##, any following token is ok, even macro argument
   *     without parentheses
   * 3 = last token was macro argument with no left parenthesis in front,
   *     only permitted when followed by ##
   * 4 = left parenthesis followed by macro argument, ok if the next token is
   *     right parenthesis or ##
   *
   * State transitions:
   * token     transitions
   * (         0->1
   * argument  0->3; 1->4
   * #         0->2
   * ##        0->2; 3->0; 4->0
   * )         4->0
   * any       0->0; 1->0; 2->0; 3->error; 4->error
   */
  void MacroDefined(const clang::IdentifierInfo *II, const clang::MacroInfo *MI) {
    clang::MacroInfo::tokens_iterator it;
    int state = 0;
    clang::SourceLocation idpos;
    const clang::IdentifierInfo *VA_ARGS = PP.getIdentifierInfo("__VA_ARGS__");

    for (it = MI->tokens_begin(); it != MI->tokens_end(); ++it) {
      if (state == 0) {
        if (it->is(clang::tok::l_paren)) {
          state = 1;
	} else if (it->is(clang::tok::hash) || it->is(clang::tok::hashhash)) {
	  state = 2;
	} else if (it->getIdentifierInfo() != VA_ARGS && isArg(MI, *it)) {
	  state = 3;
	  idpos = it->getLocation();
	}
      } else if (state == 1) {
      	if (it->getIdentifierInfo() != VA_ARGS && isArg(MI, *it)) {
	  state = 4;
	  idpos = it->getLocation();
	} else if (it->isNot(clang::tok::l_paren)) {
	  state = 0;
	}
      } else if (state == 2) {
        state = 0;
      } else if (state == 3) {
        if (it->is(clang::tok::hashhash)) {
	  state = 0;
	} else {
	  break;
	}
      } else {
        if (it->is(clang::tok::r_paren)) {
          state = 0;
	} else if (it->is(clang::tok::hashhash)) {
          state = 2;
	} else {
	  break;
	}
      }
    }

    if (state >= 3) {
      Diag.Report(clang::FullSourceLoc(idpos, mgr), NoArgParen);
    }
  }
};
}

namespace clang {
  void DoMacroCheck(Preprocessor &PP) {
    PPCallbacks *tmp = PP.getPPCallbacks();
    PPCallbacks *check = new MacroNewbie(PP, PP.getDiagnostics(), PP.getSourceManager());

    if (tmp) {
      check = new PPChainedCallbacks(check, tmp);
    }

    PP.setPPCallbacks(check);
  }
}
