//===--- Undef.cpp - Side effect safety static check ----------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the GNU General Public License.
// See LICENSE.TXT in this directory for details.
//
//===----------------------------------------------------------------------===//
//
// Static analyzer for testing side effect safety
//
//===----------------------------------------------------------------------===//

#include "clang/Basic/Diagnostic.h"
#include "clang/Basic/SourceManager.h"
#include "clang/Basic/SourceLocation.h"
#include "clang/Analysis/CFG.h"
#include "clang/Analysis/Visitors/CFGRecStmtVisitor.h"
#include "clang/Analysis/Visitors/CFGStmtVisitor.h"
#include "clang/Analysis/LocalCheckers.h"
#include "llvm/Support/Compiler.h"
#include "llvm/ADT/DenseSet.h"

using namespace clang;

namespace {
class VISIBILITY_HIDDEN ScopedMemberRef {
  ValueDecl *base;
  FieldDecl *field;
  ScopedMemberRef *parent;

public:
  ScopedMemberRef(ValueDecl *b = NULL) : base(b), field(NULL), parent(NULL) { }

  /// The object takes ownership of parent which must be dynamically
  /// created using operator new
  ScopedMemberRef(ScopedMemberRef *p, FieldDecl *f) : base(p->getBase()), field(f), parent(p) { }
  ~ScopedMemberRef() { delete parent; }

  ScopedMemberRef(const ScopedMemberRef &src) : base(src.base), field(src.field) {
    parent = src.parent ? new ScopedMemberRef(*src.parent) : NULL;
  }

  ScopedMemberRef &operator=(const ScopedMemberRef &src) {
    ValueDecl *tb = base;
    FieldDecl *tf = field;
    ScopedMemberRef *tp = parent;

    ScopedMemberRef tmp(src);

    base = tmp.base;
    field = tmp.field;
    parent = tmp.parent;

    tmp.base = tb;
    tmp.field = tf;
    tmp.parent = tp;

    return *this;
  }

  ValueDecl *getBase() const { return base; }
  ScopedMemberRef *getParent() const { return parent; }

  QualType getType() const {
    if (field && base) {
      return field->getType();
    } else if (base) {
      return base->getType();
    }

    return QualType();
  }

  friend class llvm::DenseMapInfo<ScopedMemberRef>;
};

/*! \brief Find identifier of the variable which gets modified
 */
class VISIBILITY_HIDDEN MemberRefFinder
  : public CFGStmtVisitor<MemberRefFinder, ScopedMemberRef *> {

  CFG &cfg;
  DeclRefExpr *found;
public:
  MemberRefFinder(CFG &c) : cfg(c), found(NULL) { }
  CFG& getCFG() { return cfg; }

  SourceLocation getLocation() { return found ? found->getLocStart() : SourceLocation(); }

  /// Simple variable
  ScopedMemberRef *VisitDeclRefExpr(DeclRefExpr *R) {
    found = R;
    return new ScopedMemberRef(dyn_cast<ValueDecl>(R->getDecl()));
  }

  /// struct/union member
  ScopedMemberRef *VisitMemberExpr(MemberExpr *S) {
    FieldDecl *field = dyn_cast<FieldDecl>(S->getMemberDecl());
    ScopedMemberRef *tmp = Visit(S->getBase());
    return tmp && field ? new ScopedMemberRef(tmp, field) : NULL;
  }

  /// (foo)
  ScopedMemberRef *VisitParenExpr(ParenExpr *S) {
    return Visit(S->getSubExpr());
  }

  /// &foo
  ScopedMemberRef *VisitUnaryAddrOf(UnaryOperator *S) {
    return Visit(S->getSubExpr());
  }

  /// Binary operators: ptr = ptr OR ptr +-(=) int OR int +- ptr
  ScopedMemberRef *VisitBinaryOperator(BinaryOperator *S) {
    Expr *lsub = S->getLHS()->IgnoreParens();
    Expr *rsub = S->getRHS()->IgnoreParens();
    QualType lt = lsub->getType();
    QualType rt = rsub->getType();

    // right hand side first to prevent bogus reports from
    // expressions like *(foo = bar) = baz;
    if (rt->isPointerType() || rt->isArrayType()) {
      return Visit(rsub);
    } else if (lt->isPointerType() || lt->isArrayType()) {
      return Visit(lsub);
    }

    found = NULL;
    return NULL;
  }

  /// Pointer casts. Quit on non-pointer to pointer cast
  ScopedMemberRef *VisitCastExpr(CastExpr *S) {
    Expr *sub = S->getSubExpr()->IgnoreParens();
    QualType t = sub->getType();
    if (t->isPointerType() || t->isArrayType()) {
      return Visit(sub);
    }

    found = NULL;
    return NULL;
  }

  /// Pointer casts. Quit on non-pointer to pointer cast
  ScopedMemberRef *VisitImplicitCastExpr(ImplicitCastExpr *S) {
    Expr *sub = S->getSubExpr()->IgnoreParens();
    QualType t = sub->getType();
    if (t->isPointerType() || t->isArrayType()) {
      return Visit(sub);
    }

    found = NULL;
    return NULL;
  }

  /// foo[bar]
  ScopedMemberRef *VisitArraySubscriptExpr(ArraySubscriptExpr *S) {
    return Visit(S->getBase());
  }

  /// No/unknown lvalue identifier
  ScopedMemberRef *VisitStmt(Stmt *S) {
    found = NULL;
    return NULL;
  }

#define UNARYOPERATOR(CLASS) \
  ScopedMemberRef *Visit ## CLASS(UnaryOperator *S) { \
    Expr *sub = S->getSubExpr()->IgnoreParens(); \
    QualType t = sub->getType(); \
    if (t->isPointerType() || t->isArrayType()) { \
      return Visit(sub); \
    } \
    found = NULL; \
    return NULL; \
  }

  UNARYOPERATOR(UnaryPostInc)
  UNARYOPERATOR(UnaryPostDec)
  UNARYOPERATOR(UnaryPreInc)
  UNARYOPERATOR(UnaryPreDec)
  UNARYOPERATOR(UnaryDeref)
#undef UNARYOPERATOR
};

/*! Side effect usage checker
 */
class VISIBILITY_HIDDEN SideEffectCallback
  : public CFGRecStmtVisitor<SideEffectCallback> {

  enum AccessType {
    Read,
    VolatileRead,
    Write
  };

  typedef llvm::DenseMap<ScopedMemberRef, AccessType> DMapTy;
  typedef CFGRecStmtVisitor<SideEffectCallback> Parent;

  Diagnostic &D;
  CFG &cfg;
  SourceManager &mgr;
  DMapTy VarAccess; /// Keep track of variable access types
  bool NextStatement; /// Expression test failed, next!

  // Diagnostic messages
  const unsigned NoLvalue;
  const unsigned UnsafeExpr;
  const unsigned NoBase;

protected:
  /// Check for conflict
  bool checkConflict(const ScopedMemberRef &V, SourceLocation loc, AccessType conflict = VolatileRead) {
    DMapTy::iterator it = VarAccess.find(V);

    if (it != VarAccess.end() && it->second >= conflict) {
      D.Report(FullSourceLoc(loc, mgr), UnsafeExpr);
      NextStatement = true;
      return true;
    }

    ScopedMemberRef *parent = V.getParent();

    return parent ? checkConflict(*parent, loc) : false;
  }

  /// Note variable access and generate errors on conflict
  /// (read-anything conflicts must be resolved by caller)
  void noteAccess(const ScopedMemberRef &V, AccessType t, SourceLocation loc, AccessType safeAccess = Read) {
    if (NextStatement) return;

    DMapTy::iterator it = VarAccess.find(V);

    // Report conflict
    if (it != VarAccess.end() && it->second > safeAccess) {
      D.Report(FullSourceLoc(loc, mgr), UnsafeExpr);
      NextStatement = true;
      return;
    }

    VarAccess[V] = t;
  }

  /// Unregister all VolatileRead and Read accesses to variable
  void unregisterVolatile(const ScopedMemberRef &V) {
    DMapTy::iterator it = VarAccess.find(V);

    if (it != VarAccess.end()) {
      if (it->second <= VolatileRead) {
        VarAccess.erase(it);
      } else {
        return; // stop on unregistering on write
      }
    }

    // unregister base variable access
    ScopedMemberRef *parent = V.getParent();

    if (parent) {
      unregisterVolatile(*parent);
    }
  }

public:
  SideEffectCallback(CFG &c, Diagnostic &d, SourceManager &m) : D(d), cfg(c), mgr(m), NextStatement(true),
  NoLvalue(D.getCustomDiagID(Diagnostic::Warning, "Couldn't find lvalue identifier")),
  UnsafeExpr(D.getCustomDiagID(Diagnostic::Warning, "Expression behavior may be undefined because of side effects")),
  NoBase(D.getCustomDiagID(Diagnostic::Warning, "Couldn't find base identifier for member")) { }

  CFG& getCFG() { return cfg; }

  void VisitSequencePoint(Stmt *S) {
    NextStatement = false;
    VarAccess.clear();
  }

  // VisitBlockStmts entry point
  void BlockStmt_Visit(Stmt *S) {
//    D.Report(D.getCustomDiagID(Diagnostic::Note, "New statement"));
    VisitSequencePoint(S);
    Parent::BlockStmt_Visit(S);
  }

  /// Volatile variable access is also side effect
  void VisitDeclRefExpr(DeclRefExpr *R) {
    if (NextStatement) return;

    ScopedMemberRef V(dyn_cast<ValueDecl>(R->getDecl()));

    if (R->getType().getCVRQualifiers() & QualType::Volatile) {
      noteAccess(V, VolatileRead, R->getLocStart());
    } else {
      noteAccess(V, Read, R->getLocStart());
    }
  }

  /// Any volatile member access is also side effect
  void VisitMemberExpr(MemberExpr *S) {
    VisitChildren(S);

    if (NextStatement) return;

    MemberRefFinder F(cfg);
    ScopedMemberRef *V = F.Visit(S->getBase());
    FieldDecl *field = dyn_cast<FieldDecl>(S->getMemberDecl());

    if (V && field) {
      ScopedMemberRef tmp(V, field);
      if (tmp.getType().getCVRQualifiers() & QualType::Volatile) {
        noteAccess(tmp, VolatileRead, S->getLocStart());
      } else {
        noteAccess(tmp, Read, S->getLocStart());
      }
    } else {
//      D.Report(FullSourceLoc(S->getLocStart(), mgr), NoBase);
    }

  }

  /// Process assignment operators
  void VisitBinSideEffect(BinaryOperator *S) {
    if (NextStatement) return;

    // find lvalue identifier
    MemberRefFinder F(cfg);
    const ScopedMemberRef *V = F.Visit(S->getLHS());

    // No/unrecognized lvalue identifier
    if (!V) {
      D.Report(FullSourceLoc(S->getLocStart(), mgr), NoLvalue);
      VisitChildren(S);
      return;
    // lvalue identifier has been accessed before - error
    } else if (checkConflict(*V, F.getLocation(), Read)) {
      delete V;
      return;
    }

    // check subexpressions
    Visit(S->getRHS());

    // hack to prevent some safe volatile assignments from being
    // reported as unsafe
    unregisterVolatile(*V);

    Visit(S->getLHS());
    VarAccess[*V] = Write;
    delete V;
  }

  /// Process increments and decrements
  void VisitUnarySideEffect(UnaryOperator *S) {
    if (NextStatement) return;

    // find lvalue identifier
    MemberRefFinder F(cfg);
    const ScopedMemberRef *V = F.Visit(S->getSubExpr());

    // No/unrecognized lvalue identifier
    if (!V) {
      D.Report(FullSourceLoc(S->getLocStart(), mgr), NoLvalue);
      VisitChildren(S);
      return;
    // lvalue identifier has been accessed before - conflict
    } else if (checkConflict(*V, F.getLocation(), Read)) {
      delete V;
      return;
    }

    // check subexpressions
    VisitChildren(S);
    noteAccess(*V, Write, F.getLocation(), VolatileRead);
    delete V;
  }

  /// Pass throught the entire statement subtree
  void VisitStmt(Stmt *S) {
    if (NextStatement) return;
    VisitChildren(S);
  }

#define BINARYSIDEEFFECT(CLASS) \
  void Visit ## CLASS(BinaryOperator *S) { \
    VisitBinSideEffect(S); \
  }

  BINARYSIDEEFFECT(BinAssign)
  BINARYSIDEEFFECT(AssignOperator)
  BINARYSIDEEFFECT(CompoundAssignOperator)
#undef BINARYSIDEEFFECT

#define UNARYSIDEEFFECT(CLASS) \
  void Visit ## CLASS(UnaryOperator *S) { \
    VisitUnarySideEffect(S); \
  }

  UNARYSIDEEFFECT(UnaryPostInc)
  UNARYSIDEEFFECT(UnaryPostDec)
  UNARYSIDEEFFECT(UnaryPreInc)
  UNARYSIDEEFFECT(UnaryPreDec)
#undef UNARYSIDEEFFECT
};
}

namespace llvm {
/*! \brief ScopedMemberRef class info for DenseMap/DenseSet
 */
template<>
class DenseMapInfo<ScopedMemberRef> {
public:
  /// Empty key
  static inline ScopedMemberRef getEmptyKey() { return ScopedMemberRef(); }

  /// Key for deleted items
  static inline ScopedMemberRef getTombstoneKey() {
    ScopedMemberRef tmp;
    tmp.field = reinterpret_cast<FieldDecl*>(-1);
    return tmp;
  }

  /// Hash function
  static unsigned getHashValue(const ScopedMemberRef &key) {
    return DenseMapInfo<ScopedMemberRef*>::getHashValue(key.parent) ^ DenseMapInfo<ValueDecl*>::getHashValue(key.base);
  }

  /// Equality test
  static bool isEqual(const ScopedMemberRef &l, const ScopedMemberRef &r) {
    return l.base == r.base && l.field == r.field && 
      ((l.parent != NULL && r.parent != NULL &&
      isEqual(*l.parent, *r.parent)) || l.parent == r.parent);
  }

  /// Always call copy constructor when copying
  static bool isPod() { return false; }
};
}

namespace clang {
/// Execute side effect check on code block
void CheckSideEffects(CFG& cfg, Diagnostic &Diags, SourceManager &mgr) {
  SideEffectCallback C(cfg, Diags, mgr);
  cfg.VisitBlockStmts(C);
}
}
