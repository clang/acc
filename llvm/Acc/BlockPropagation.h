//===--- BlockPropagation.h - Implicit definition dataflow solver ----------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the GNU General Public License.
// See LICENSE.TXT in parent directory for details.
//
//===----------------------------------------------------------------------===//
//
// Dataflow solver for implicit definition problems
//
//===----------------------------------------------------------------------===//

#include <cstring> // delete me when DenseMap.h gets fixed
#include "llvm/ADT/DenseMap.h"
#include <vector>
#include <set>

namespace llvm {
  class Value;
  class Constant;
  class Argument;
  class Instruction;
  class PHINode;
  class TerminatorInst;
  class BasicBlock;
  class Function;
}

namespace acc {
class BlockSolver;

class BlockLatticeFunction {
public:
  typedef void *LatticeVal;

private:
  LatticeVal UndefVal, SafeVal, UnsafeVal, UntrackedVal;

public:
  BlockLatticeFunction(LatticeVal undef, LatticeVal safe, LatticeVal unsafe,
    LatticeVal untracked) : UndefVal(undef), SafeVal(safe), UnsafeVal(unsafe),
    UntrackedVal(untracked) { }
  virtual ~BlockLatticeFunction() { }

  LatticeVal getUndefVal() const { return UndefVal; }
  LatticeVal getSafeVal() const { return SafeVal; }
  LatticeVal getUnsafeVal() const { return UnsafeVal; }
  LatticeVal getUntrackedVal() const { return UntrackedVal; }

  /// IsUntrackedValue - true = this Value* is not interesting, ignore
  virtual bool IsUntrackedVal(llvm::Value *V) {
    return false;
  }

  /// ComputeConstant - get lattice value for constant
  virtual LatticeVal ComputeConstant(llvm::Constant *C) {
    return getUnsafeVal();
  }

  /// ComputeArgument - get lattice value for function argument
  virtual LatticeVal ComputeArgument(llvm::Argument *I) {
    return getUnsafeVal();
  }

  /// MergeValues - merge two lattice values into one (for in-block merges)
  virtual LatticeVal MergeValues(LatticeVal X, LatticeVal Y) {
    return getUnsafeVal();
  }

  /// PHIMerge - merge two lattice values into one (for edge merges)
  virtual LatticeVal PHIMerge(LatticeVal X, LatticeVal Y) {
    return getUnsafeVal();
  }

  /// ComputeInstructionState - get lattice value for intstruction
  virtual LatticeVal ComputeInstructionState(llvm::Instruction &I, BlockSolver &BS) {
    return getUnsafeVal();
  }

  /// ValuePassThrough - Used to get state through values which are not
  /// interesting on their own (eg. passing pointer state through
  /// GetElementPtr when GEPs are untracked)
  virtual LatticeVal ValuePassThrough(llvm::Value *Val, llvm::BasicBlock *BB, BlockSolver &BS) {
    return getUnsafeVal();
  }

  /// ValuePropagates - true = this value needs propagation through CFG
  virtual bool ValuePropagates(LatticeVal V) {
    return V != getUndefVal() && V != getUntrackedVal();
  }

  virtual void PrintValue(LatticeVal V, std::ostream &OS);
};

/// Control flow sensitive dataflow solver
/// Premises:
/// - Lattice value of instruction result is independent on lattice values
///   of instruction arguments
/// - There are implicit defines created by uses of the result
/// - Implicit defines can be modeled using lattice merges
/// - LatticeFunc->MergeValues(x,x) == x
/// - LatticeFunc->MergeValues(undef,x) == x
class BlockSolver {
public:
  typedef BlockLatticeFunction::LatticeVal LatticeVal;
private:
  typedef std::pair<llvm::Value*, llvm::BasicBlock*> PropChange;
  typedef llvm::DenseMap<llvm::Value*, LatticeVal> ValueStateMap;

  BlockLatticeFunction *LatticeFunc;
  llvm::DenseMap<llvm::BasicBlock*, ValueStateMap> BlockState;
  std::vector<PropChange> PropWorklist;
  std::set<llvm::PHINode*> PHIList;

  // DO NOT IMPLEMENT:
  BlockSolver(const BlockSolver&);
  void operator=(const BlockSolver&);

public:
  BlockSolver(BlockLatticeFunction *LF) : LatticeFunc(LF) { }

  /// getLatticeState - get state for value in selected basic block
  LatticeVal getLatticeState(llvm::Value *Val, llvm::BasicBlock *BB);

  /// Solve - run solver on function
  void Solve(llvm::Function &F);

  /// UpdateState - change state of value in selected basic block, scheduling
  /// propagation if needed
  void UpdateState(llvm::Value *Val, LatticeVal V, llvm::BasicBlock *BB);
  void Print(std::ostream &OS);

private:
  /// Propagate - propagate state of value down starting in selected basic block
  void Propagate(llvm::Value *Val, llvm::BasicBlock *BB);

  /// PropagatePHI - like Propagate but recalculate modified PHI nodes this time
  void PropagatePHI(llvm::Value *Val, llvm::BasicBlock *BB);

  /// CalculatePhiState - merge incoming value states into PHI node state
  LatticeVal CalculatePHIState(llvm::PHINode *Phi);
};
}
