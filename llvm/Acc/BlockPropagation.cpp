//===--- BlockPropagation.cpp - Implicit definition dataflow solver --------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the GNU General Public License.
// See LICENSE.TXT in parent directory for details.
//
//===----------------------------------------------------------------------===//
//
// Dataflow solver for implicit definition problems
//
//===----------------------------------------------------------------------===//

#include "llvm/Support/InstIterator.h"
#include "llvm/Support/CFG.h"
#include "llvm/Function.h"
#include "llvm/Instructions.h"
#include "BlockPropagation.h"
#include <iostream>

namespace acc {
void BlockLatticeFunction::PrintValue(BlockLatticeFunction::LatticeVal V, std::ostream &OS) {
  if (V == UndefVal) {
    OS << "undefined";
  } else if (V == SafeVal) {
    OS << "safe";
  } else if (V == UnsafeVal) {
    OS << "unsafe";
  } else if (V == UntrackedVal) {
    OS << "untracked";
  } else {
    OS << "unknown";
  }
}

void BlockSolver::Solve(llvm::Function &F) {
  // compute argument states and schedule propagation
  for (llvm::Function::arg_iterator it = F.arg_begin(), end = F.arg_end(); it != end; ++it) {
    if (LatticeFunc->IsUntrackedVal(&*it)) {
      continue;
    }

    UpdateState(&*it, LatticeFunc->ComputeArgument(&*it), &F.getEntryBlock());
  }

  // compute instruction states and schedule propagation
  for (llvm::inst_iterator it = llvm::inst_begin(F), end = llvm::inst_end(F); it != end; ++it) {
    // compute first to create implicit defines
    LatticeVal V = LatticeFunc->ComputeInstructionState(*it, *this);

    if (LatticeFunc->IsUntrackedVal(&*it)) {
      continue;
    }

    // compute PHIs later
    if (llvm::PHINode *ptr = llvm::dyn_cast<llvm::PHINode>(&*it)) {
//      std::cerr << "Adding " << ptr->getName() << " to PHI list\n";
      PHIList.insert(ptr);
      continue;
    }

    UpdateState(&*it, V, it->getParent());
  }

  // propagate lattice values through control flow graph
  while (!PropWorklist.empty()) {
    llvm::Value *Val = PropWorklist.back().first;
    llvm::BasicBlock *BB = PropWorklist.back().second;
    PropWorklist.pop_back();
    Propagate(Val, BB);
  }

  for (std::set<llvm::PHINode*>::iterator it = PHIList.begin(), end = PHIList.end(); it != end; ++it) {
    LatticeVal V = CalculatePHIState(*it);

    if (LatticeFunc->ValuePropagates(V)) {
      UpdateState(*it, V, (*it)->getParent());
//      BlockState[(*it)->getParent()][*it] = V;
//      PropWorklist.push_back(PropChange(*it, (*it)->getParent()));
    }
  }

  // propagate the rest of lattice values through control flow graph
  while (!PropWorklist.empty()) {
    llvm::Value *Val = PropWorklist.back().first;
    llvm::BasicBlock *BB = PropWorklist.back().second;
    PropWorklist.pop_back();
    PropagatePHI(Val, BB);
  }

  // the remaining PHINode lattice values should not propagate by now
  for (std::set<llvm::PHINode*>::iterator it = PHIList.begin(), end = PHIList.end(); it != end; ++it) {
    UpdateState(*it, CalculatePHIState(*it), (*it)->getParent());
//    BlockState[(*it)->getParent()][*it] = CalculatePHIState(*it);
  }
  PHIList.clear();
}

void BlockSolver::Propagate(llvm::Value *Val, llvm::BasicBlock *BB) {
//  std::cerr << "Propagating value " << Val->getName() << "\n";
  ValueStateMap::iterator I = BlockState[BB].find(Val);
  if (I == BlockState[BB].end() || !LatticeFunc->ValuePropagates(I->second)) {
    return;
  }

  LatticeVal V = I->second;
  std::vector<llvm::BasicBlock*> Worklist;

  Worklist.push_back(BB);

  while (!Worklist.empty()) {
    llvm::BasicBlock *curBB = Worklist.back();
    Worklist.pop_back();

    for (llvm::succ_iterator it = succ_begin(curBB), end = succ_end(curBB); it != end; ++it) {
      I = BlockState[*it].find(Val);
      LatticeVal V2;

      if (I != BlockState[*it].end()) {
        V2 = LatticeFunc->MergeValues(V, I->second);

        if (V2 == I->second) {
          continue;
	}
      } else {
        V2 = V;
      }

      // keep propagating this value
      if (V2 == V) {
        BlockState[*it][Val] = V2;
        Worklist.push_back(*it);
      // schedule other lattice values for later propagation
      } else {
        UpdateState(Val, V2, *it);
      }
    }
  }
}

void BlockSolver::PropagatePHI(llvm::Value *Val, llvm::BasicBlock *BB) {
  Propagate(Val, BB);

  for (llvm::Value::use_iterator it = Val->use_begin(), end = Val->use_end(); it != end; ++it) {
    if (llvm::PHINode *ptr = llvm::dyn_cast<llvm::PHINode>(*it)) {
      LatticeVal V = CalculatePHIState(ptr);

      if (LatticeFunc->ValuePropagates(V)) {
        UpdateState(ptr, V, ptr->getParent());
      }
    }
  }
}

void BlockSolver::UpdateState(llvm::Value *Val, LatticeVal V, llvm::BasicBlock *BB) {
  ValueStateMap &map = BlockState[BB];
  ValueStateMap::iterator I = map.find(Val);

  // in-block lattice merge if needed
  if (I != map.end()) {
    V = LatticeFunc->MergeValues(V, I->second);
  }

  if (I != map.end() && I->second == V) {
    return;
  }

//  std::cerr << "Setting value " << Val->getName() << " in block " << BB->getName() << " to ";
//  LatticeFunc->PrintValue(V, std::cerr);
//  std::cerr << "\n";
  map[Val] = V;

  if (LatticeFunc->ValuePropagates(V)) {
    PropWorklist.push_back(PropChange(Val, BB));
  }
}

BlockSolver::LatticeVal BlockSolver::getLatticeState(llvm::Value *V, llvm::BasicBlock *BB) {
  ValueStateMap::iterator I = BlockState[BB].find(V);
  return I == BlockState[BB].end() ? LatticeFunc->getUndefVal() : I->second;
}

BlockSolver::LatticeVal BlockSolver::CalculatePHIState(llvm::PHINode *Phi) {
  LatticeVal ret = LatticeFunc->getUndefVal();

  // FIXME: untracked state pass-through (GEP)
  for (unsigned i = 0; i < Phi->getNumIncomingValues(); i++) {
    LatticeVal V;

    if (LatticeFunc->IsUntrackedVal(Phi->getIncomingValue(i))) {
      V = LatticeFunc->ValuePassThrough(Phi->getIncomingValue(i), Phi->getIncomingBlock(i), *this);
    } else {
      V = getLatticeState(Phi->getIncomingValue(i), Phi->getIncomingBlock(i));
    }

    ret = LatticeFunc->PHIMerge(ret, V);
  }

  return ret;
}

void BlockSolver::Print(std::ostream &OS) {
  for (llvm::DenseMap<llvm::BasicBlock*, ValueStateMap>::iterator it = BlockState.begin(); it != BlockState.end(); ++it) {
    OS << "Basic block " << it->first->getName() << "\n";

    for (ValueStateMap::iterator it2 = it->second.begin(); it2 != it->second.end(); ++it2) {
      OS << "  Value " << it2->first->getName() << ": ";
      LatticeFunc->PrintValue(it2->second, OS);
      OS << "\n";
    }
  }
}
}
