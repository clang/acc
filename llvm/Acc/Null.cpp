//===--- Null.cpp - Null pointer dereference check ------------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the GNU General Public License.
// See LICENSE.TXT in parent directory for details.
//
//===----------------------------------------------------------------------===//
//
// Runtime check generator for NULL pointer dereference
//
//===----------------------------------------------------------------------===//

#define DEBUGTYPE "null"
#include <iostream>
#include "llvm/Pass.h"
#include "llvm/Function.h"
#include "llvm/Constants.h"
#include "BlockPropagation.h"
#include "llvm/Instructions.h"
#include "llvm/IntrinsicInst.h"
#include "llvm/Support/CFG.h"
#include "llvm/Module.h"
#include "llvm/Transforms/Scalar.h"

namespace {
	/// get rid of GetElementPtrs
	llvm::Value *stripGEP(llvm::Value *valptr) {
		llvm::GetElementPtrInst *gep;
		do {
			gep = llvm::dyn_cast<llvm::GetElementPtrInst>(valptr);
			valptr = gep ? gep->getPointerOperand() : valptr;
		} while(gep);

		return valptr;
	}

	/// Lattice function for NULL pointer dereference frontier problem
	class LatticeNull : public acc::BlockLatticeFunction {
	private:
		char ptr[4];
		LatticeVal validate;

	public:
		typedef acc::BlockLatticeFunction::LatticeVal LatticeVal;
		LatticeNull() : acc::BlockLatticeFunction(ptr, ptr+1, ptr+2,
			NULL), validate(ptr+3) { }

		LatticeVal getValidateVal() { return validate; }

		bool IsUntrackedVal(llvm::Value *V) {
			return !llvm::isa<llvm::PointerType>(V->getType()) ||
				llvm::isa<llvm::GetElementPtrInst>(V);
		}

		LatticeVal ComputeConstant(llvm::Constant *C) {
			return getUntrackedVal();
		}

		llvm::Constant *GetConstant(LatticeVal LV, llvm::Value *Val, acc::BlockSolver &SS) {
			return NULL;
		}

		LatticeVal ComputeArgument(llvm::Argument *I) {
			return IsUntrackedVal(I) ? getUntrackedVal() : getUnsafeVal();
		}

		LatticeVal MergeValues(LatticeVal X, LatticeVal Y) {
			if (X == getUntrackedVal()) {
				assert(Y == getUntrackedVal() && "Tracking error");
				return getUntrackedVal();
			}

			assert(Y != getUntrackedVal() && "Tracking error");

			if (X == getUndefVal() || Y == getUndefVal()) {
				return X == getUndefVal() ? Y : X;
			} else if (X == validate || Y == validate) {
				return validate;
			} else if (X == getSafeVal()) {
				return Y == getUnsafeVal() ? validate : getSafeVal();
			} else if (Y == getSafeVal()) {
				return X == getUnsafeVal() ? validate : getSafeVal();
			} else {
				return getUnsafeVal();
			}
		}

		LatticeVal PHIMerge(LatticeVal X, LatticeVal Y) {
			if (X == getUntrackedVal()) {
				assert(Y == getUntrackedVal() && "Tracking error");
				return getUntrackedVal();
			}

			assert(Y != getUntrackedVal() && "Tracking error");

			if (X == getUndefVal() || Y == getUndefVal()) {
				return X == getUndefVal() ? Y : X;
			} else if (X == getUnsafeVal() || Y == getUnsafeVal()) {
				return getUnsafeVal();
			} else {
				return getSafeVal();
			}

		}

		LatticeVal ComputeInstructionState(llvm::Instruction &I, acc::BlockSolver &SS) {
			llvm::Value *valptr = NULL;
			llvm::Instruction *inst = llvm::dyn_cast<llvm::Instruction>(stripGEP(&I));

			if (!inst) {
				return getSafeVal();
			}

			if (llvm::isa<llvm::AllocaInst>(inst)) {
				return getSafeVal();
			} else if (llvm::LoadInst *ptr = llvm::dyn_cast<llvm::LoadInst>(inst)) {
				valptr = ptr->getPointerOperand();
			} else if (llvm::StoreInst *ptr = llvm::dyn_cast<llvm::StoreInst>(inst)) {
				valptr = ptr->getPointerOperand();
			}

			if (valptr) {
				valptr = stripGEP(valptr);
				assert(!llvm::isa<llvm::ConstantPointerNull>(valptr) && "Null pointer dereference");

				SS.UpdateState(valptr, getSafeVal(), inst->getParent());
			}

			return IsUntrackedVal(&I) ? getUntrackedVal() : getUnsafeVal();
		}

		LatticeVal ValuePassThrough(llvm::Value *Val, llvm::BasicBlock *BB, acc::BlockSolver &BS) {
			return BS.getLatticeState(stripGEP(Val), BB);
		}

		bool ValuePropagates(LatticeVal V) {
			return V == getUnsafeVal();
		}

		void PrintValue(LatticeVal V, std::ostream &OS) {
			if (V == validate) {
				OS << "to be validated";
			} else {
				BlockLatticeFunction::PrintValue(V, OS);
			}
		}
	};

	/// CheckNull Runtime check code generator
	class CheckNull {
	private:
		llvm::LLVMContext &Context;
		llvm::Module &M;
		// Checklist - NULL test for each pointer
		// PHIList - GetElementPtr to PHINode pointer arithmetic
		// replacement mapping
		llvm::DenseMap<llvm::Value*, llvm::Value*> Checklist, PHIList;
		// Blocklist - Mapping of new blocks to old blocks
		llvm::DenseMap<llvm::BasicBlock*, llvm::BasicBlock*> Blocklist;
		// ValidPtr - list of pointers validated in current basic block
		std::set<llvm::Value*> ValidPtr;
		// Crash generator stuff
		llvm::DbgStopPointInst *stoppoint;
		llvm::BasicBlock *lineblock, *funcblock;
		llvm::Function *Func;
		// Dataflow solver stuff
		LatticeNull LF;
		acc::BlockSolver *BS;

		llvm::Value *stripGEP(llvm::Value *ptr) {
			ptr = ::stripGEP(ptr);
			llvm::DenseMap<llvm::Value*, llvm::Value*>::iterator I = PHIList.find(ptr);
			return I == PHIList.end() ? ptr : I->second;
		}

		llvm::BasicBlock *getBlock(llvm::BasicBlock *BB) {
			llvm::DenseMap<llvm::BasicBlock*, llvm::BasicBlock*>::iterator I = Blocklist.find(BB);
			return I == Blocklist.end() ? BB : I->second;
		}

		/// getNullCheck - get or create (ptr != NULL) value
		llvm::Value *getNullCheck(llvm::Value *ptr) {
			ptr = this->stripGEP(ptr);

			llvm::DenseMap<llvm::Value*, llvm::Value*>::iterator I = Checklist.find(ptr);

			if (I != Checklist.end()) {
				return I->second;
			}

			llvm::BasicBlock::iterator inspos;
			llvm::BasicBlock *insblock;

			if (llvm::isa<llvm::Argument>(ptr)) {
				insblock = &Func->getEntryBlock();
				inspos = llvm::BasicBlock::iterator(*insblock->getFirstNonPHI());
			} else if (llvm::Instruction *p2 = llvm::dyn_cast<llvm::Instruction>(ptr)) {
				insblock = p2->getParent();
				inspos = llvm::BasicBlock::iterator(*p2);
				++inspos;
			} else if (llvm::isa<llvm::ConstantPointerNull>(ptr)) {
				assert(0 && "Requested check for constant");
			} else {
				assert(0 && "Unknown internal operand type");
			}

			llvm::Value *ret;
			
			if (llvm::PHINode *p2 = llvm::dyn_cast<llvm::PHINode>(ptr)) {
				llvm::PHINode *phi = llvm::PHINode::Create(llvm::IntegerType::get(1), ptr->getName() + ".validate", inspos);
				Checklist[ptr] = phi;
				phi->reserveOperandSpace(p2->getNumIncomingValues());

				for (unsigned i = 0; i < p2->getNumIncomingValues(); i++) {
					phi->addIncoming(getNullCheck(p2->getIncomingValue(i)), p2->getIncomingBlock(i));
				}

				ret = phi;
			} else {
				ret = new llvm::ICmpInst(inspos, llvm::CmpInst::ICMP_NE, ptr, Context.getConstantPointerNull(llvm::dyn_cast<llvm::PointerType>(ptr->getType())), ptr->getName() + ".validate");
				Checklist[ptr] = ret;
			}

			return ret;
		}

		/// getStringConstant - get or create named string constant
		llvm::GlobalVariable *getStringConstant(std::string name, std::string val) {
			llvm::GlobalVariable *ret = M.getNamedGlobal(name);

			if (ret) {
				return ret;
			}

			llvm::Constant *arr = Context.getConstantArray(val);
			return new llvm::GlobalVariable(M, arr->getType(), true, llvm::GlobalValue::InternalLinkage, arr, name);
		}

		/// cloneStringConstant - get or create new name for string
		/// constant
		llvm::GlobalVariable *cloneStringConstant(std::string name, llvm::Value *orig) {
			llvm::GlobalVariable *ret = M.getNamedGlobal(name);

			if (ret) {
				return ret;
			}

			llvm::ConstantExpr *con = llvm::dyn_cast<llvm::ConstantExpr>(orig);
			llvm::GlobalVariable *tmp = llvm::dyn_cast<llvm::GlobalVariable>(con->getOperand(0));
			return new llvm::GlobalVariable(M, tmp->getInitializer()->getType(), true, llvm::GlobalValue::InternalLinkage, tmp->getInitializer(), name);
		}

		bool nullGEP(llvm::GetElementPtrInst *pos) {
			llvm::Value *ptr = this->stripGEP(pos);

			// make sure it really needs validation
			acc::BlockSolver::LatticeVal val = BS->getLatticeState(ptr, getBlock(pos->getParent()));
			if (val != LF.getValidateVal() && val != LF.getUnsafeVal()) {
				return false;
			}

			// already validated in this basic block
			if (ValidPtr.find(ptr) != ValidPtr.end()) {
				return false;
			}

			llvm::Value::use_iterator UI = pos->use_begin();
			for (; UI != pos->use_end(); ++UI) {
				if (llvm::StoreInst *store = llvm::dyn_cast<llvm::StoreInst>(*UI)) {
					// pointer is stored into memory
					// let's mess it up
					if (store->getOperand(0) == pos) {
						break;
					}
				} else if (!llvm::isa<llvm::LoadInst>(*UI) && !llvm::isa<llvm::GetElementPtrInst>(*UI)) {
					break;
				}
			}

			// this pointer is only read, stored into and indexed,
			// there's no need to mess it up
			if (UI == pos->use_end()) {
				return false;
			}


			// break the GEP into separate basic block
			llvm::BasicBlock *prevblock = pos->getParent();
			llvm::BasicBlock *gepblock = prevblock->splitBasicBlock(pos, pos->getName() + ".gep");
			llvm::BasicBlock::iterator it = pos;
			llvm::BasicBlock *contblock = gepblock->splitBasicBlock(++it, pos->getName() + ".cont");
			Blocklist[gepblock] = getBlock(prevblock);
			Blocklist[contblock] = getBlock(prevblock);

			llvm::PHINode *phi = llvm::PHINode::Create(pos->getType(), pos->getName() + ".phi", &contblock->front());
			llvm::Value* check = getNullCheck(ptr);
			prevblock->getInstList().pop_back();
			llvm::BranchInst::Create(gepblock, contblock, check, prevblock);

			// and switch it to PHI(GEP, NULL) according to
			// NULL test of its pointer operand
			pos->replaceAllUsesWith(phi);
			phi->reserveOperandSpace(2);
			phi->addIncoming(pos, gepblock);
			phi->addIncoming(llvm::ConstantPointerNull::get(pos->getType()), prevblock);

			// insert PHI into translation tables
			Checklist[phi] = check;
			PHIList[phi] = ptr;

			return true;
		}

		/// validatePtr - generate runtime check at selected position
		bool validatePtr(llvm::Instruction *pos, llvm::Value *ptr) {
			ptr = this->stripGEP(ptr);

			// make sure it really needs validation
			if (BS->getLatticeState(ptr, getBlock(pos->getParent())) != LF.getValidateVal()) {
				return false;
			}

			// already validated in this basic block
			if (ValidPtr.find(ptr) != ValidPtr.end()) {
				return false;
			}

			// prepare basic blocks
			llvm::Value *check = getNullCheck(ptr);
			llvm::BasicBlock *invblock, *prevblock = pos->getParent();

			llvm::BasicBlock *contblock = prevblock->splitBasicBlock(pos, ptr->getName() + ".valid");
			Blocklist[contblock] = getBlock(prevblock);

			if (!stoppoint) {
				if (!funcblock) {
					// prepare error block for the function
					// there's no debug info to read
					funcblock = llvm::BasicBlock::Create(std::string("ptr.invalid"), Func, contblock);
					llvm::Value *idxs[] = {Context.getConstantInt(llvm::Type::Int32Ty, 0), Context.getConstantInt(llvm::Type::Int32Ty, 0)};
					llvm::SmallVector<llvm::Value*, 2> args;
					args.push_back(llvm::ConstantExpr::getGetElementPtr(getStringConstant("acc.nullderef", "Null pointer dereference"), idxs, 2));
					args.push_back(llvm::ConstantExpr::getGetElementPtr(getStringConstant("acc.funcname_" + Func->getName(), Func->getName()), idxs, 2));
					llvm::Function *barf = llvm::dyn_cast<llvm::Function>(M.getOrInsertFunction("__acc_runtime_failure", llvm::Type::VoidTy, args[0]->getType(), args[1]->getType(), NULL));
					barf->addFnAttr(llvm::Attribute::NoReturn | llvm::Attribute::NoUnwind);
					llvm::CallInst::Create<llvm::SmallVector<llvm::Value*,2>::iterator>(barf, args.begin(), args.end(), "", funcblock);
					new llvm::UnreachableInst(funcblock);
				}

				invblock = funcblock;
			} else if (!lineblock) {
				// prepare line error block from debug info
				lineblock = llvm::BasicBlock::Create(std::string("ptr.invalid"), Func, contblock);
				llvm::Value *idxs[] = {Context.getConstantInt(llvm::Type::Int32Ty, 0), Context.getConstantInt(llvm::Type::Int32Ty, 0)};
				llvm::SmallVector<llvm::Value*, 4> args;
				args.push_back(llvm::ConstantExpr::getGetElementPtr(getStringConstant("acc.nullderef", "Null pointer dereference"), idxs, 2));
				args.push_back(llvm::ConstantExpr::getGetElementPtr(getStringConstant("acc.funcname_" + Func->getName(), Func->getName()), idxs, 2));
				args.push_back(llvm::ConstantExpr::getGetElementPtr(cloneStringConstant("acc.filename", stoppoint->getFileName()), idxs, 2));
				args.push_back(stoppoint->getLineValue());
				llvm::Function *barf = llvm::dyn_cast<llvm::Function>(M.getOrInsertFunction("__acc_runtime_failure_line", llvm::Type::VoidTy, args[0]->getType(), args[1]->getType(), args[2]->getType(), args[3]->getType(), NULL));
				barf->addFnAttr(llvm::Attribute::NoReturn | llvm::Attribute::NoUnwind);
				llvm::CallInst::Create<llvm::SmallVector<llvm::Value*,2>::iterator>(barf, args.begin(), args.end(), "", lineblock);
				new llvm::UnreachableInst(lineblock);
				invblock = lineblock;
			} else {
				invblock = lineblock;
			}

			// insert branch which tests for NULL
			prevblock->getInstList().pop_back();
			llvm::BranchInst::Create(contblock, invblock, check, prevblock);

			// ptr is validated in this block from now on
			ValidPtr.insert(ptr);
			return true;
		}
	public:
		CheckNull(llvm::Module &m) : Context(llvm::getGlobalContext()),
			M(m), stoppoint(NULL), lineblock(NULL),
			funcblock(NULL) { }

		/// runOnFunction - generate check in function
		virtual bool runOnFunction(llvm::Function &F) {
			bool ret = false, tmp;
			acc::BlockSolver Solver(&LF);

			Func = &F;
			BS = &Solver;
			Solver.Solve(F);


			for (llvm::Function::iterator it = F.begin(); it != F.end(); ++it) {
				for (llvm::BasicBlock::iterator it2 = it->begin(), end2 = it->end(); it2 != end2; ++it2) {
					tmp = false;
					if (llvm::StoreInst *ptr = llvm::dyn_cast<llvm::StoreInst>(&*it2)) {
						ret = validatePtr(ptr, ptr->getPointerOperand()) || ret;
						it2 = ptr;
						it = ptr->getParent();
						end2 = it->end();
					} else if (llvm::LoadInst *ptr = llvm::dyn_cast<llvm::LoadInst>(&*it2)) {
						ret = validatePtr(ptr, ptr->getPointerOperand()) || ret;
						it2 = ptr;
						it = ptr->getParent();
						end2 = it->end();
					} else if (llvm::GetElementPtrInst *ptr = llvm::dyn_cast<llvm::GetElementPtrInst>(&*it2)) {
						if(nullGEP(ptr)) {
							// This is not the basic block we're looking for, move along...
							it = ptr->getParent();
							++it;
							it2 = it->begin();
							end2 = it->end();
							ret = true;
						}

					} else if (llvm::DbgStopPointInst *ptr = llvm::dyn_cast<llvm::DbgStopPointInst>(&*it2)) {
						stoppoint = ptr;
						lineblock = NULL;
					}
				}

				ValidPtr.clear();
			}

			Checklist.clear();
			funcblock = NULL;
			lineblock = NULL;
			stoppoint = NULL;
			BS = NULL;


			return ret;
		}
	};

	/// NullPass - generate checks for module
	class NullPass : public llvm::ModulePass {
	public:
		static char ID;

		NullPass() : llvm::ModulePass(&ID) {}

		bool runOnModule(llvm::Module &M) {
			CheckNull pass(M);
			bool ret = false;

			for (llvm::Module::iterator it = M.begin(); it != M.end(); ++it) {
				if (it->isDeclaration()) {
					continue;
				}

//				getAnalysisID<llvm::FunctionPass>(llvm::PromoteMemoryToRegisterID, *it);
				ret = pass.runOnFunction(*it) || ret;
			}

			return ret;
		}

		void getAnalysisUsage(llvm::AnalysisUsage &AU) const {
			// This pass needs pure SSA, it doesn't work
			// on load/store code
//			AU.addRequiredID(llvm::PromoteMemoryToRegisterID);
			AU.addPreservedID(llvm::PromoteMemoryToRegisterID);
			AU.addPreservedID(llvm::BreakCriticalEdgesID);
			AU.addPreservedID(llvm::LowerAllocationsID);
			AU.addPreservedID(llvm::LowerSwitchID);
		}
	};
}

char NullPass::ID = 0;
static llvm::RegisterPass<NullPass> X("null", "Test NULL dereference");

namespace llvm {
const PassInfo *const NullID = &X;

ModulePass *createNullPass() {
	return new NullPass();
}
}
