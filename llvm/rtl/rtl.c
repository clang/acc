//===--- rtl.c - ACC runtime library --------------------------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the GNU General Public License.
// See LICENSE.TXT in parent directory for details.
//
//===----------------------------------------------------------------------===//
//
// Simple helper functions for ACC runtime checks
//
//===----------------------------------------------------------------------===//

#include <stdio.h>
#include <stdlib.h>

/// __acc_runtime_failure - print message and die
void __acc_runtime_failure(const char *error, const char *func) {
	fprintf(stderr, "Runtime check failure in function %s: %s\n", func,
	error);
	abort();
}

/// __acc_runtime_failure_line - print message containing filename and line
/// number and die
void __acc_runtime_failure_line(const char *error, const char *func,
	const char *file, int line) {

	fprintf(stderr, "Runtime check failure in function %s (%s:%d): %s\n",
		func, file, line, error);
	abort();
}
